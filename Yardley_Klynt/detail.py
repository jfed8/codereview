from django.conf import settings
from django_mako_plus import view_function
from django.http import HttpResponseRedirect
from catalog import models as cmod

from .. import dmp_render, dmp_render_to_string



@view_function
def process_request(request):
    pid = request.urlparams[0]
    try:
        product = cmod.Product.objects.get(id=pid)
    except cmod.Product.DoesNotExist:
        return HttpResponseRedirect('/catalog/index/')

    #Add currentProd to last5 list
    if product.id in request.last5: #if already in the list, move to the top
        position = request.last5.index(product.id)
        request.last5.insert(0, request.last5.pop(position))
    else: #else, simply add to top of list
        request.last5.insert(0, product.id)

    context = {
        'product' : product
    }
    return dmp_render(request, 'detail.html', context)
