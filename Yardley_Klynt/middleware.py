from catalog import models as cmod

def Last5ProductsMiddleware(get_response):
    # One-time configuration and initialization.

    def middleware(request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        #Get last5 product.ids from session
        request.last5 = request.session.get('last5')
        if request.last5 is None: #if none, set as empty list.
            request.last5 = []


        ####GET Product objects to display#######
        request.last5products = []
        for prod_id in request.last5:
            temp = cmod.Product.objects.get(id=prod_id)
            request.last5products.append(temp)



        # let Django continue the processing, Django Calls view
        response = get_response(request)
        # Code to be executed for each request/response after


        #Save to session
        request.session['last5'] = request.last5[:5]


        return response

    return middleware
