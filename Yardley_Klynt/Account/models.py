# ACCOUNT MODELS
from django.db import models
from django.contrib.auth.models import AbstractUser

# Define models here 

CONTACT_CHOICES = [
    ['text', 'Text'],
    ['email', 'Email'],
    ['voice', 'Voice'],
]

class User(AbstractUser):
    #password
    #last_login
    #username
    #first_name
    #last_name
    #email
    #is_staff
    #is_active
    #date_joined

    address = models.TextField(null=True, blank=True)
    city = models.TextField(null=True, blank=True)
    state = models.TextField(null=True, blank=True)
    zipcode = models.TextField(null=True, blank=True)
    phone = models.TextField(null=True, blank=True)
    pref_contact = models.TextField(null=True, blank=True, choices = CONTACT_CHOICES)
    birthday = models.DateField(null=True, blank=True)
    gender = models.TextField(null=True, blank=True)
