import os, os.path, sys
from django.core import management
from django.db import connection
from datetime import date, datetime

# ensure the user really wants to do this
areyousure = input('''
  You are about to drop and recreate the entire database.
  All data are about to be deleted.  Use of this script
  may cause itching, vertigo, dizziness, tingling in
  extremities, loss of balance or coordination, slurred
  speech, temporary zoobie syndrome, longer lines at the
  testing center, changed passwords in Learning Suite, or
  uncertainty about whether to call your professor
  'Brother' or 'Doctor'.

  Please type 'yes' to confirm the data destruction: ''')
if areyousure.lower() != 'yes':
    print()
    print('  Wise choice.')
    sys.exit(1)

# initialize the django environment
os.environ['DJANGO_SETTINGS_MODULE'] = 'fomo.settings'
import django
django.setup()


# drop and recreate the database tables
print()
print('Living on the edge!  Dropping the current database tables.')
with connection.cursor() as cursor:
    cursor.execute("DROP SCHEMA public CASCADE")
    cursor.execute("CREATE SCHEMA public")
    cursor.execute("GRANT ALL ON SCHEMA public TO postgres")
    cursor.execute("GRANT ALL ON SCHEMA public TO public")

# make the migrations and migrate
management.call_command('makemigrations')
management.call_command('migrate')

#imports for our project
from account.models import User


#Initialize Dummy Data

#User1 'Klynt Yardley'
u1 = User()
u1.username = 'kyardley'
u1.password = '123!@#qweQWE'
u1.last_login = datetime.now()
u1.first_name = 'Klynt'
u1.last_name = 'Yardley'
u1.email = 'klynt.yardley@gmail.com'
u1.is_staff = True
u1.is_active = True
u1.date_joined = date(2001, 1, 1)
u1.address = '830 N 100 W'
u1.city = 'Provo'
u1.state = 'UT'
u1.zipcode = '84604'
u1.phone = '4355313456'
u1.pref_contact = 'text'
u1.birthday = date(1993, 9, 10)
u1.gender = 'M'

u1.save()

#User2 'Bonnie Yardley'
u2 = User()
u2.username = 'byardley'
u2.password = '123!@#qweQWE'
u2.last_login = datetime.now()
u2.first_name = 'Bonnie'
u2.last_name = 'Yardley'
u2.email = 'bonnie.yardley@gmail.com'
u2.is_staff = False
u2.is_active = True
u2.date_joined = date(2001, 1, 1)
u2.address = '123 Street Ave'
u2.city = 'Las Vegas'
u2.state = 'NV'
u2.zipcode = '72168'
u2.phone = '702111111'
u2.pref_contact = 'text'
u2.birthday = date(1992, 2, 11)
u2.gender = 'F'
u2.save()

#User3 'Lynn Yardley'
u3 = User()
u3.username = 'lyardley'
u3.password = '123!@#qweQWE'
u3.last_login = datetime.now()
u3.first_name = 'Lynn'
u3.last_name = 'Yardley'
u3.email = 'lynn.yardley@gmail.com'
u3.is_staff = False
u3.is_active = True
u3.date_joined = date(2001, 1, 1)
u3.address = '270 W 200 S'
u3.city = 'Beaver'
u3.state = 'UT'
u3.zipcode = '84713'
u3.phone = '4354211759'
u3.pref_contact = 'email'
u3.birthday =  date(1992, 2, 11)
u3.gender = 'M'
u3.save()

#User4 'Melissa Yardley'
u4 = User()
u4.username = 'myardley'
u4.password = '123!@#qweQWE'
u4.last_login = datetime.now()
u4.first_name = 'Melissa'
u4.last_name = 'Yardley'
u4.email = 'klynt.yardley@gmail.com'
u4.is_staff = False
u4.is_active = True
u4.date_joined = date(2001, 1, 1)
u4.address = '270 W 200 S'
u4.city = 'Beaver'
u4.state = 'UT'
u4.zipcode = '84713'
u4.phone = '4354211758'
u4.pref_contact = 'voice'
u4.birthday  =  date(1992, 2, 11)
u4.gender = 'F'
u4.save()

'''

# Queries all users and prints first_name + last_name
users = User.objects.all()
for u in users:
    print(u.first_name, u.last_name)

# Filters user 'Bonnie' and prints birthday
bonnie = User.objects.filter(first_name = 'Bonnie')
for b in bonnie:
    print(b.birthday)

# Filters all Staff
StaffUsers = User.objects.filter(is_staff = True)
for user in StaffUsers:
    print(user.first_name + " is Staff")

#Excludes all staff
customers = User.objects.exclude(is_staff = True)
for c in customers:
    print(c.first_name, 'is a customer')

'''
