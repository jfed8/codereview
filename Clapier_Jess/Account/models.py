from django.db import models
from django.contrib.auth.models import AbstractUser

# Define models here
# ACCOUNT MODELS

CONTACT_CHOICES = [
    ['text', 'Text'],
    ['email', 'Email'],
    ['voice', 'Voice'],
]

class FomoUser(AbstractUser):
    # password
    # last_login
    # username
    # email
    # first_name
    # last_name
    # is_staff
    # is_active
    # date_joined
    birthdate = models.DateField(null=True)
    gender = models.TextField(null=True)
    address = models.TextField(null=True, blank=True)
    city = models.TextField(null=True, blank=True)
    state = models.TextField(null=True, blank=True)
    zipcode = models.TextField(null=True, blank=True)
    pref_contact = models.TextField(null=True, blank=True, choices=CONTACT_CHOICES)
