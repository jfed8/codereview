from django.conf import settings
from django.http import HttpResponseRedirect
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string
from catalog import models as cmod

@view_function
def process_request(request):
    pid = request.urlparams[0]
    try:
        product = cmod.Product.objects.get(id=pid)
    except cmod.Product.DoesNotExist:
        return HttpResponseRedirect('/catalog/index/')

    # add to the last 5 viewed items
    if product.id not in request.last5:
        request.last5.insert(0, product.id)
    else:
        request.last5.remove(product.id)
        request.last5.insert(0, product.id)
    print('>>>>>', product.id)

    while len(request.last5) > 6:
        request.last5.pop()

    last5products = []
    for p in request.last5:
        temp = cmod.Product.objects.get(id=p)
        print('>>>>>>', temp.name)
        last5products.append(temp)


    return dmp_render(request, 'detail.html', context = {
        'product':product,
        'last5products': last5products,
    })
