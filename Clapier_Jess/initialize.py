import os
from datetime import datetime, date

#initialize the django environment
os.environ['DJANGO_SETTINGS_MODULE'] = 'fomo.settings'
import django
django.setup()

from account.models import FomoUser

#USER 1 'Michael Scott'

u1 = FomoUser()
u1.password = 'mscott'
u1.last_login = datetime.now()
u1.username = 'mscott'
u1.email = 'mscott@gmail.com'
u1.first_name = 'Michael'
u1.last_name = 'Scott'
u1.is_staff = True
u1.is_active = True
u1.date_joined = datetime.now()
u1.birthdate = date(1980, 9, 15)
u1.gender = 'male'
u1.address = '555 S. State Street'
u1.city = 'Scranton'
u1.state = 'PA'
u1.zipcode = '12345'
u1.pref_contact = 'text'
u1.save()

#USER 2 'Creed Braton'

u2 = FomoUser()
u2.password = 'cbraton'
u2.last_login = datetime.now()
u2.username = 'cbraton'
u2.email = 'cbraton@gmail.com'
u2.first_name = 'Creed'
u2.last_name = 'Braton'
u2.is_staff = False
u2.is_active = True
u2.date_joined = datetime.now()
u2.birthdate = date(1970, 10, 21)
u2.gender = 'male'
u2.address = '123 S. Main Street'
u2.city = 'Brownx'
u2.state = 'NY'
u2.zipcode = '54321'
u2.pref_contact = 'voice'
u2.save()

#USER 3 'Jim Halpert'

u3 = FomoUser()
u3.password = 'jhalpert'
u3.last_login = datetime.now()
u3.username = 'jhalpert'
u3.email = 'jhalpert@gmail.com'
u3.first_name = 'Jim'
u3.last_name = 'Halpert'
u3.is_staff = True
u3.is_active = True
u3.date_joined = datetime.now()
u3.birthdate = date(1989, 1, 21)
u3.gender = 'male'
u3.address = '600 S. Paper Drive'
u3.city = 'Scranton'
u3.state = 'PA'
u3.zipcode = '12345'
u3.pref_contact = 'email'
u3.save()

#USER 4 'Dwight Schrute'

u4 = FomoUser()
u4.password = 'dschrute'
u4.last_login = datetime.now()
u4.username = 'dschrute'
u4.email = 'dschrute@gmail.com'
u4.first_name = 'Dwight'
u4.last_name = 'Schrute'
u4.is_staff = True
u4.is_active = True
u4.date_joined = datetime.now()
u4.birthdate = date(1986, 7, 11)
u4.gender = 'male'
u4.address = '100 N. Beet Street'
u4.city = 'Scranton'
u4.state = 'PA'
u4.zipcode = '33333'
u4.pref_contact = 'text'
u4.save()

# run queries
u = FomoUser.objects.get(id=1) #brings back single result
print(u.username)

#.filter returns a list
# id___lt, id__lte, id__gt, id__gte
# username__startswith, username__endswith
# first_name__contains
# objects.exclude

staff = FomoUser.objects.filter(is_staff = True)
for s1 in staff:
    print(s1.username)

empfilt = FomoUser.objects.filter(id__lt=3)
for e1 in empfilt:
    print(e1.username, e1.id)

empexcl = FomoUser.objects.exclude(username__startswith='j')
for e1 in empexcl:
    print(e1.username, e1.id)
