import os
from datetime import datetime, date

#initialize the django environment
os.environ['DJANGO_SETTINGS_MODULE'] = 'fomo.settings'
import django
django.setup()


from account.models import User


# User 1 - Jaden Feddock

u1 = User()
u1.password = 'awesome'
u1.last_login = datetime.now()
u1.username = 'jadenf'
u1.first_name = 'Jaden'
u1.last_name = 'Feddock'
u1.email = 'jadenfeddock@test.com'
u1.is_staff = True
u1.is_active = True
u1.date_joined = datetime.now()
u1.birthdate = date(1994, 8, 30)
u1.gender = 'Male'
u1.address = '1027 W 1033 N #107'
u1.city = 'Orem'
u1.state = 'Utah'
u1.zipcode = '84057'
u1.pref_contact = 'text'


# User 2 - Madeleine Feddock

u2 = User()
u2.password = 'ilovejaden'
u2.last_login = datetime.now()
u2.username = 'madeleinef'
u2.first_name = 'Madeleine'
u2.last_name = 'Feddock'
u2.email = 'madeleine@test.com'
u2.is_staff = True
u2.is_active = True
u2.date_joined = datetime.now()
u2.birthdate = date(1997, 10, 20)
u2.gender = 'Female'
u2.address = '1027 W 1033 N #107'
u2.city = 'Orem'
u2.state = 'Utah'
u2.zipcode = '84057'
u2.pref_contact = 'telegram'


# User 3 - Carter Feddock

u3 = User()
u3.password = 'newbaby'
u3.last_login = datetime.now()
u3.username = 'carterf'
u3.first_name = 'Carter'
u3.last_name = 'Feddock'
u3.email = 'carter@test.com'
u3.is_staff = False
u3.is_active = True
u3.date_joined = datetime.now()
u3.birthdate = date(2016, 1, 1)
u3.gender = 'Male'
u3.address = '1027 W 1033 N #107'
u3.city = 'Orem'
u3.state = 'Utah'
u3.zipcode = '84057'
u3.pref_contact = 'email'


# User 4 - Brighton Feddock

u4 = User()
u4.password = 'newbaby2'
u4.last_login = datetime.now()
u4.username = 'brightonf'
u4.first_name = 'Brighton'
u4.last_name = 'Feddock'
u4.email = 'brighton@test.com'
u4.is_staff = False
u4.is_active = True
u4.date_joined = datetime.now()
u4.birthdate = date(2016, 1, 2)
u4.gender = 'Female'
u4.address = '1027 W 1033 N #107'
u4.city = 'Orem'
u4.state = 'Utah'
u4.zipcode = '84057'
u4.pref_contact = 'voice'


# Save Data

u1.save()
u2.save()
u3.save()
u4.save()


# Query Results

Users = User.objects.all()
for user in Users:
    print(user.first_name)

StaffUsers = User.objects.filter(is_staff = True)
for user in StaffUsers:
    print(user.first_name + " is Staff")

OldUsers = User.objects.exclude(birthdate__gt=date(2015, 1, 1))
for user in OldUsers:
    print(user.first_name + " is Old")
