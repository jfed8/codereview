from decimal import Decimal

@view_function
def record_sale(user, shopping_items, cart_subtotal, cart_tax, cart_shipping, cart_total, shipping_address, stripe_charge):

    sale = Sale()
    sale.user = user
    sale.shipping_address = shipping_address
    sale.subtotal = cart_subtotal
    sale.tax = cart_tax
    sale.shipping = cart_shipping
    sale.total = cart_total
    sale.save()

    for item in shopping_items:
        saleitem = SaleItem()
        saleitem.sale = sale
        saleitem.product = item.product
        saleitem.quantity = item.quantity
        item_price = item.product.price * item.quantity
        saleitem.sale_price = round(Decimal(item_price), 2)
        saleitem.save()

    payment = Payment()
    payment.sale = sale
    payment.stripe_chargeid = stripe_charge.id
    payment.amount = Decimal(stripe_charge.amount) / Decimal('100')
    payment.amount_refunded = stripe_charge.amount_refunded
    payment.currency = stripe_charge.currency
    payment.description = stripe_charge.description
    payment.paid = stripe_charge.paid
    payment.save()

    return sale
