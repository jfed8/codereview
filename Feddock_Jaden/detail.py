from django.conf import settings
from django_mako_plus import view_function
from datetime import datetime
from django import forms
from catalog import models as cmod
from formlib.form import FormMixIn
from django.http import HttpResponseRedirect
from .. import dmp_render, dmp_render_to_string
from django.contrib.auth.decorators import login_required, permission_required
import random


@view_function
def process_request(request):

    # create a form to edit the product information
    # initial data will be the product info
    # if is_vaid()
    #   set form cleaned_data into the fields of product
    #   product.save()

    try:
        product = cmod.Product.objects.get(id=request.urlparams[0])
    except cmod.Product.DoesNotExist:
        # What now?
        return HttpResponseRedirect('/catalog/index/')


    if product.id in request.last5:
        position = request.last5.index(product.id)
        request.last5.insert(0, request.last5.pop(position))
    else:
        request.last5.insert(0, product.id)

    while len(request.last5) > 5:
        request.last5.pop()



    context = {
        'product' : product,
    }
    return dmp_render(request, 'detail.html', context)
