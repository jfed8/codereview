from catalog import models as cmod

def Last5ProductsMiddleware(get_response):

    def middleware(request):

        print('>>> beginning of Middleware function')

        request.last5 = request.session.get('last5')
        print('>>>>>> ', request.last5)


        if request.last5 is None:
            request.last5 = []


        request.last5products = []
        for p in request.last5:
            temp = cmod.Product.objects.get(id=p)
            request.last5products.append(temp)

        response = get_response(request)


        request.session['last5'] = request.last5[:5]

        print('>>>>>> ', request.last5)




        return response


    return middleware
