from catalog import models as cmod

def Last5ProductsMiddleware(get_response):

    def middleware(request):
        #process request
        print('>>>>>>>> Beginning of Middleware')
        request.last5 = request.session.get('last5')
        if request.last5 is None:
            request.last5 = []

        request.last5products = []
        for p in request.last5:
            temp = cmod.Product.objects.get(id=p)
            request.last5products.append(temp)

        response = get_response(request)


        #process response
        print('!!!!!! Ending of middleware function')

        #ANOTHER OPTION for getting last 5 products
        request.session['last5'] = request.last5[:5]

        return response
    return middleware
