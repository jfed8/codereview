#purpose is to just put dummy data inside
import os
from datetime import datetime

#initialize the django environment
os.environ['DJANGO_SETTINGS_MODULE'] = 'fomo.settings'
import django
django.setup()

from account.models import fomoUser

# u1 = fomoUser()
# u1.username = 'dschrute'
# u1.first_name = 'Dwight'
# u1.last_name = 'Schrute'
# u1.email = 'dschrute@gmail.com'
# u1.is_staff = False
# u1.is_active = True
# u1.date_joined = datetime.now()
# u1.password = 'beetfarmer1'
# u1.last_login = datetime.now()
# u1.birthdate = datetime(1980, 9, 15)
# u1.gender = 'male'
# u1.save()
#
# u2 = fomoUser()
# u2.username = 'mscott'
# u2.first_name = 'Michael'
# u2.last_name = 'Scott'
# u2.email = 'mscott@gmail.com'
# u2.is_staff = True
# u2.is_active = True
# u2.date_joined = datetime.now()
# u2.password = 'worldsbestboss'
# u2.last_login = datetime.now()
# u2.birthdate = datetime(1980, 9, 16)
# u2.gender = 'male'
# u2.save()
#
# u3 = fomoUser()
# u3.username = 'pbeasley'
# u3.first_name = 'Pamela'
# u3.last_name = 'Beasley'
# u3.email = 'pbeasley@gmail.com'
# u3.is_staff = False
# u3.is_active = False
# u3.date_joined = datetime.now()
# u3.password = 'iheartjim'
# u3.last_login = datetime.now()
# u3.birthdate = datetime(1980, 9, 17)
# u3.gender = 'female'
# u3.save()
#
# u4 = fomoUser()
# u4.username = 'jhalpert'
# u4.first_name = 'Jim'
# u4.last_name = 'Halpert'
# u4.email = 'jhalpert@gmail.com'
# u4.is_staff = False
# u4.is_active = True
# u4.date_joined = datetime.now()
# u4.password = 'slackerlife'
# u4.last_login = datetime.now()
# u4.birthdate = datetime(1983, 8, 15)
# u4.gender = 'male'
# u4.save()

print()
tempu = fomoUser.objects.get(id=8)
print('Username:', tempu.username, 'Password:', tempu.password)
print()

for c in fomoUser.objects.all():
    print(c.first_name, c.last_name)
print()

users = fomoUser.objects.filter(is_staff=True)
for u in users:
    print(u.first_name, u.last_name, 'is a', u.gender, 'manager')
print()

oldpeople = fomoUser.objects.exclude(birthdate__lt=datetime(1981, 1, 1))
for o in oldpeople:
    print(o.first_name, o.last_name, 'was born after 1981')
print()
