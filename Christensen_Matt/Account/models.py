from django.db import models
from django.contrib.auth.models import AbstractUser

class fomoUser(AbstractUser):
    # username
    # first_name
    # last_name
    # email
    # is_staff
    # is_active
    # date_joined
    # password
    # last_login
    birthdate = models.DateField(null=True)
    gender = models.TextField(null=True)
