from django.conf import settings
from django_mako_plus import view_function
from catalog import models as cmod
from django.http import HttpResponseRedirect
from catalog import models as cmod

from .. import dmp_render, dmp_render_to_string

@view_function
def process_request(request):
    pid = request.urlparams[0]
    try:
        product = cmod.Product.objects.get(id=pid)
    except cmod.Product.DoesNotExist:
        return HttpResponseRedirect('/catalog/cust_products/')


    if product.id in request.last5:
        request.last5.remove(product.id)
    # add to the last 5 viewed items

    while len(request.last5) > 5:
        request.last5.pop()
    # what if this item was already in the list?!?!?!?!?!!!!!!!

    request.last5.insert(0, product.id)

    while len(request.last5) > 5:
        request.last5.pop()
    print('<<<<<MADE IT TO DETAIL')

    return dmp_render(request, 'detail.html', context = {
        'product': product,
    })
